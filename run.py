#!/usr/bin/env python
import sys, os

def usage():
    print "Usage: python run.py [notebook]"
    sys.exit(1)

if __name__ == "__main__":
    
    # check for usage
    if len(sys.argv) > 2:
        usage()

    # run notebook
    if len(sys.argv) == 1 or sys.argv[1] == 'notebook':
        cmd = 'ipython notebook --pylab=inline notebooks'
        os.system(cmd)
    else:
        usage()


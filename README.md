Collective Dynamics of Small-World and Scale-Free Networks
==========================================================

In this set of IPython notebooks, we recreate the simulations and
major results and figures from famous papers dealing with network/graph
analysis of large-scale graphs.  We currently look at:

- Watts & Strogatz seminal 1998 paper on small-world properties of networks.  
- Albert & Barbasi 2002 introduction of Scale-Free preferential attachment algorithm.

References
----------

- Watts, D.J. & Strogatz, S.H. Collective dynamics of "small-world" networks, Nature 393, 440-442 (1998).  [Nature](http://www.nature.com/nature/journal/v393/n6684/abs/393440a0.html)  [Paper at Strogatz Web](http://www.stevenstrogatz.com/publications.html)
- Albert, R. & Barabasi, A.L. Statistical mechanics of complex networks, Reviews of Modern Physics 74(1), 47-97 (2002). 